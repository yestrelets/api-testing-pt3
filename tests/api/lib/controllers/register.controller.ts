import { ApiRequest } from "../request";

export class RegisterController {
    async registerUser(userData: object) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body(userData)
            .send();
        return response;
    }
}