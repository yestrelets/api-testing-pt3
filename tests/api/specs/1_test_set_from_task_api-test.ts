import { expect } from 'chai';
import { RegisterController } from '../lib/controllers/register.controller';
import { AuthController } from '../lib/controllers/auth.controller';
import { UsersController } from '../lib/controllers/users.controller';

const register = new RegisterController();
const auth = new AuthController();
const users = new UsersController();

const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Test Set from Task', () => {
  let accessToken: string;
  let userId: number;

  it('should return 200 status code when register a new user', async () => {
    const userData = {
      avatar: 'string',
      email: 'example@test.com',
      userName: 'string',
      password: 'string',
    };

    const response = await register.registerUser(userData);

    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(
      response.timings.phases.total,
      `Response time should be less than 1s`,
    ).to.be.lessThan(1000);
  });

  it(`should return 200 status code and all users when getting the user collection`, async () => {
    let response = await users.getAllUsers();

    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(
      response.timings.phases.total,
      `Response time should be less than 1s`,
    ).to.be.lessThan(1000);
    expect(
      response.body.length,
      `Response body should have more than 1 item`,
    ).to.be.greaterThan(1);
    expect(response.body).to.be.jsonSchema(schemas.schema_allUsers);
  });

  it('should return 200 status code and user data when login', async () => {
    const response = await auth.login('example@test.com', 'string');

    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(
      response.timings.phases.total,
      `Response time should be less than 1s`,
    ).to.be.lessThan(1000);
    expect(response.body).to.be.jsonSchema(schemas.schema_login);

    accessToken = response.body.token.accessToken.token;
    userId = response.body.user.id;
  });

  it('should return 200 status code and user data when get user data', async () => {
    const response = await users.getUserFromToken(accessToken);

    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(
      response.timings.phases.total,
      `Response time should be less than 1s`,
    ).to.be.lessThan(1000);
    expect(response.body).to.be.jsonSchema(schemas.schema_user);
  });

  it('should return 200 status code when update user data', async () => {
    const userData = {
      id: userId,
      avatar: 'string',
      email: 'example@test.com',
      userName: 'string2',
    };

    const response = await users.updateUser(userData, accessToken);

    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(
      response.timings.phases.total,
      `Response time should be less than 1s`,
    ).to.be.lessThan(1000);
  });

  it('should return 200 status code and user data when get user data', async () => {
    const response = await users.getUserFromToken(accessToken);

    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(
      response.timings.phases.total,
      `Response time should be less than 1s`,
    ).to.be.lessThan(1000);
    expect(response.body.userName).to.be.equal('string2');
    expect(response.body).to.be.jsonSchema(schemas.schema_user);
  });

  it('should return 200 status code and user data when get user by id', async () => {
    const response = await users.getUserById(userId);

    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(
      response.timings.phases.total,
      `Response time should be less than 1s`,
    ).to.be.lessThan(1000);
    expect(response.body).to.be.jsonSchema(schemas.schema_user);
  });

  it('should return 200 status code when delete user', async () => {
    const response = await users.deleteUser(accessToken, userId);

    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(
      response.timings.phases.total,
      `Response time should be less than 1s`,
    ).to.be.lessThan(1000);
  });
});
